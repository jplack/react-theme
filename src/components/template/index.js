import React from 'react';
import Header from './partials/Header';
import Footer from './partials/Footer';

const Template = props => {
  return (
    <>
    <Header />
      {props.children}
    <Footer />
    </>
  )
};
export default Template;