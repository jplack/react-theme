import React from 'react';
import styled from 'styled-components';
import '../../../../style.scss';
import colors from '../../../theme/colors.json';
import fonts from '../../../theme/fonts.json'


const Footer = styled('footer')`
  height: 40px;
  flex: 1 100%;
  background: linear-gradient(90deg, ${colors.primary} 0%, transparent 50%);
  font-family: ${fonts.sans}
`

const FooterComponent = props => {
  return (
    <Footer>Footer</Footer>
  )
};
export default FooterComponent;