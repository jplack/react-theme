import React from 'react';
import styled from 'styled-components';

const Header = styled('header')`
  height: ${props => props.height + 'px' || '40px'};
  flex: 1 100%;
  background: ${props => props.background || 'linear-gradient(270deg, aquamarine 0%, transparent 50%)'};
`


const HeaderComponent = props => {
  let headerHeight = ReactTheme.customizer_options.headerHeight
  let headerBg = ReactTheme.customizer_options.headerBgColor
  let showHeader =ReactTheme.customizer_options.showHeader

  let siteTitle = ReactTheme.title
  let tagline = ReactTheme.description

  const [header, setHeader] = React.useState({height: headerHeight, bg: headerBg, show: showHeader});
  React.useEffect(() => {
    function receiveMessage(event) {
      console.log('receiveMessage')
      const data = JSON.parse(event.data).data;
      if (data) {
        switch (data[0]) {
          case 'headerHeight':
              if ( data[1] > 0 && data[1] !== header.height ) {
                // console.log(data[1])
                setHeader({...header, height: data[1]})
              }
            break;
          case 'headerBgColor':
              if( data[1] !== header.bg) {
  
                setHeader({...header, bg: data[1]})
              }
            break;
          case 'showHeader':
            if( data[1] !== header.show) {
              setHeader({...header, show: data[1]})
            }
        }
      }
  }


  window.addEventListener('message', receiveMessage);
    return () => {
    window.removeEventListener('message', receiveMessage);
    };
  });



    // const val = JSON.parse(event.data).data[1]
    // if ( val > 0 ) {
    //   setHeader({height: JSON.parse(event.data).data[1]})
    // }
 // setHeight(headerHeight);

  // console.log(header)
  if (header.show && header.show !=='false') {
    return (
      <Header background={header.bg} height={header.height}>
      <h1>{siteTitle}</h1>
      <h3>{tagline}</h3>
      </Header>
    )
  } else return (<></>);
};
export default HeaderComponent;