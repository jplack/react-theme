import React from 'react';

const SingleColumn = props => {
  return (
    <main>
      {props.children}
    </main>
  )
};

export default SingleColumn;