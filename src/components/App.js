import React from 'react';
import deepFreeze from 'deep-freeze';
import { ThemeProvider } from 'styled-components';
import Template from './template';
import SingleColumn from './template/partials/SingleColumn';

const App = () => {
 deepFreeze(ReactTheme);

 fetch(ReactTheme.api_url + 'settings', {headers: {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': '*',
  'Access-Control-Allow-Credentials': 'true',
   'X-WP-Nonce': ReactTheme.nonce
 }}).then(res => res.json()).then(data => console.log(data)) 
  return (
  <Template>
    <SingleColumn>
      Page content
    </SingleColumn>
  </Template>
)};

export default App;