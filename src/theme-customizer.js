/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and 
 * then make any necessary changes to the page using jQuery.
 */


  console.log('theme-customizer')
  var reactApp = document.getElementsByTagName("iframe")[0].contentWindow;
   wp.customize('headerHeight', function(control) {
     control.bind(function onChange( controlValue ) {
      console.log('sendHeight')
          reactApp.postMessage(controlValue, "*")
      });
   });

   wp.customize('headerBgColor', function(control) {
    control.bind(function( controlValue ) {
        reactApp.postMessage(controlValue, "*")
    });
  });

    wp.customize('showHeader', function(control) {
      control.bind(function( controlValue ) {
          reactApp.postMessage(controlValue, "*")
      });
    });
	
