<?php

 // Exit if accessed directly
 defined( 'ABSPATH' ) || exit;

 require 'vendor/autoload.php';

class ReactTheme {
  
    public function bootstrapWordPress() {
      $Customizer = new Customizer;
      add_action('customize_register', [ $Customizer, 'registerCustomizer']);
      add_filter( 'rest_url_prefix', [ $this, 'themeApiSlug' ]);
      add_action( 'wp_enqueue_scripts', [ $this, 'themeScripts' ] );
      //Customizer::bootstrapWordPress();

      add_action( 'customize_preview_init', [$this, 'customizer_live_preview'] );
      add_action( 'customize_controls_enqueue_scripts', [$this, 'customizer_controls'] );
    }

    public function customizer_live_preview() {
      wp_enqueue_script( 
          'themecustomizer',			//Give the script an ID
          get_template_directory_uri().'/src/theme-customizer.js',//Point to file
          array( 'jquery','customize-preview' ),	//Define dependencies
          '',						//Define a version (optional) 
          true						//Put script in footer?
      );
    }

    public function customizer_controls() {
      wp_enqueue_script( 
          'themecustomizer-controls',			//Give the script an ID
          get_template_directory_uri().'/src/customizer-controls.js',//Point to file
          array( 'jquery','customize-controls' ),	//Define dependencies
          '',						//Define a version (optional) 
          true						//Put script in footer?
      );
    }

    public function themeScripts() {
        wp_enqueue_style( 'react-theme-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
        wp_register_script( 'react-theme-app', get_stylesheet_directory_uri() . '/dist/index.js', [], '1.0', true );
        wp_enqueue_script( 'react-theme-app' );
  
        $currentUser = get_the_ID();
        $userData    = get_userdata( $currentUser );
  
        $passedValues = array( 
            'title'        => get_bloginfo( 'name', 'display' ),
            'description'  => get_bloginfo( 'description', 'display' ),
            'api_url'      => esc_url_raw( preg_replace( '/\/wp/', '', get_rest_url( null, '/wp/v2/' ), 1 ) ),
            'nonce'        => wp_create_nonce( 'wp_rest' ),
            'user'         => $userData,
            'customizer_options'  => get_theme_mods()
        );
        wp_localize_script( 'react-theme-app', 'ReactTheme', $passedValues );
    }

    public function themeApiSlug() {
        return 'rest';
    }


  
}

function reactThemeInit() {
  $reactTheme = new ReactTheme();
  $reactTheme->bootstrapWordPress();

  return $reactTheme; 
}

reactThemeInit();

