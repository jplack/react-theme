const scssToJson = require('scss-to-json');
const path = require('path');
const fs = require('fs');

var inputFiles = fs.readdirSync(path.resolve(__dirname, '../sass/variables'));
inputFiles.forEach(file => {
  if (file === '_variables.scss' || file === '_fonts_import.scss') return;
  
  let vars = scssToJson(path.resolve(__dirname, '../sass/variables/' + file));
  
  const varType = file.slice(1).slice(0, -6);
  const name = file.slice(1).slice(0, -5);
  
  let varKeys = Object.keys(vars);
  let values = {};
  
  varKeys.forEach(key => {
    let newKey = key.replace('$'+ varType + '_', '')
    values[newKey] = vars[key];
  }); 

  const outputFile = path.resolve(__dirname, `../src/theme/${name}.json`);

  fs.writeFileSync(outputFile, JSON.stringify(values, null, 2));
});

console.log('SCSS vars converted to JSON');
process.exit();