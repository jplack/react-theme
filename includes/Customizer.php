<?php

class Customizer {
    public static function bootstrapWordPress() {
        add_action( 'customize_register', [ $this, 'registerCustomizer' ] );
    }
  
    public function registerCustomizer( $wp_customize ) {
        $wp_customize->add_section( 'header_section',
            array(
                'title' => 'Header',
                'description' => 'Adjust your Header section.',
                'panel' => '', // Only needed if adding your Section to a Panel
                'priority' => 160, // Not typically needed. Default is 160
                'capability' => 'edit_theme_options', // Not typically needed. Default is edit_theme_options
                'description_hidden' => 'false', // Rarely needed. Default is False
            )
        );
        $wp_customize->add_setting( 'headerHeight',
            array(
                'default' => '150',
                'transport'         => 'postMessage',
                'sanitize_callback' => 'sanitize_text_field'
            )
        );
        $wp_customize->add_control( 'headerHeight',
            array(
                'label' => 'Header height',
                'description' => esc_html__( 'Controls the height of your main site header' ),
                'section' => 'header_section',
                'priority' => 10, // Optional. Order priority to load the control. Default: 10
                'type' => 'text', // Can be either text, email, url, number, hidden, or date
                'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
                'input_attrs' => array( // Optional.
                    'class' => 'react-theme-options',
                    'style' => 'border: 1px solid rebeccapurple',
                    'placeholder' => '150',
                ),
            )
        );
        $wp_customize->add_setting('headerBgColor', 
            array(
                'default'           => 'fff',
                'sanitize_callback' => 'sanitize_hex_color',
                'capability'        => 'edit_theme_options',
                'transport'         => 'postMessage',
            )
        );
        $wp_customize->add_control( new WP_Customize_Color_Control(
            $wp_customize, 'headerBgColor', 
                array(
                    'label'    => 'Background Color',
                    'section'  => 'header_section',
                    'settings' => 'headerBgColor',
                    'input_attrs' => array( // Optional.
                        'class' => 'react-theme-options',
                    ),
                )
            )
        );
        $wp_customize->add_setting('showHeader', array(
            'default'        => 'true',
            'capability'     => 'edit_theme_options',
            'transport'         => 'postMessage',
            'sanitize_callback' => 'react_sanitize_show_header_option',
        ));
        $wp_customize->add_control('showHeader', array(
            'label'      => 'Show Header',
            'section'    => 'header_section',
            'settings' => 'showHeader',
            'type'       => 'radio',
            'choices'    => array(
                'true' => 'Show',
                'false' => 'Hide',
            ),
            'input_attrs' => array( // Optional.
                'class' => 'react-theme-options',
            ),
        ));

        // $wp_customize->selective_refresh->add_partial( 'headerHeight',
        //     array(
        //         'selector' => 'header',
        //         'container_inclusive' => false,
        //         'render_callback' => function() {
        //             echo mytheme_get_social_media_icons();
        //         },
        //         'fallback_refresh' => true
        //     )
        //     );
    }
 
}

function react_sanitize_show_header_option( $choice ) {
	$valid = array(
		'true',
		'false',
	);

	if ( in_array( $choice, $valid, true ) ) {
		return $choice;
	}

	return 'true';
}